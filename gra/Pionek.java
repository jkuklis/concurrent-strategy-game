package gra;

public class Pionek implements Postać {
	private final int wysokość;
	private final int szerokość;
	
	public Pionek(int wysokość, int szerokość) {
		this.wysokość = wysokość;
		this.szerokość = szerokość;
	}
	
	public int dajWysokość() {
		return wysokość;
	}
	
	public int dajSzerokość() {
		return szerokość;
	}
	
}

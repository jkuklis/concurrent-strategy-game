package gra;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class MojaPlansza implements Plansza {
	
	// jeżeli debug ustawiona na true, drukowana jest reprezentacja planszy
	boolean debug = false;
	
	// lokalna klasa używana do przechowywania informacji o postaci
	class Krotka {
		
		// postać, informacje o której przechowujemy
		private Postać postać;
		
		// numer wiersza, w którym znajdują się skrajnie górne pozycje zajmowane przez postać
		private int wiersz;
		
		// numer kolumny, w której znajdują się skrajnie lewe pozycje zajmowane przez postać
		private int kolumna;
		
		// identyfikator postaci
		private int id;
		
		// kierunek ruchu, który postać chciała ostatnio wykonać
		private Kierunek ruch;
		
		// zmienna mówiąca, czy postaci udało się wykonać ostatni ruch,
		// który chciała wykonać
		private boolean wykonał = true;
		
		private Krotka(Postać postać, int wiersz, int kolumna, int id) {
		    assert(postać != null);
		    assert(0 <= id);
		    
			this.postać = postać;
		    this.wiersz = wiersz;
		    this.kolumna = kolumna;
		    this.id = id;
		}
		
		// konstruktor tymczasowej krotki, która ma być przepisana na inną
		private Krotka() {}
		
		// funkcja na potrzeby debugowania
		public String toString() {
			StringBuilder tmp = new StringBuilder();
			tmp.append(id + " " + wiersz + " " + kolumna + " " + postać.dajWysokość() 
					+ " " + postać.dajSzerokość() + " " + ruch + " " + wykonał + " ");
			return tmp.toString();
		}		
	}
	
	// wysokość planszy
	private int wysokość;
	// szerokość planszy
	private int szerokość;
	
	// lista krotek przechowujących informacje o postaciach umieszczonych na mapie
	private final static List<Krotka> naPlanszy = new ArrayList<Krotka>();
	
	// liczba mówiąca, ile postaci wyraziło chęć ustawienia się na planszy
	private static int ilePostaci = 0;
	
	// mapa postaci 'odwiedzonych' przy przechodzeniu dfs'em po grafie zależności
	// postaci, przy czym krawędź prowadzi od postaci A do postaci B, jeżeli
	// postać A czeka na ruch postaci B
	private final static Set<Integer> odwiedzeni = new HashSet<Integer>();
	
	// zmienna mówiąca, czy doszło do deadlock'u
	private boolean deadlock = false;

	private boolean mieściSię(Krotka k) {
		return (k.wiersz >= 0 && k.wiersz + k.postać.dajWysokość() <= wysokość
				&& k.kolumna >= 0 && k.kolumna + k.postać.dajSzerokość() <= szerokość);
	}
	
	// sprawdza, czy postacie z krotek k1, k2 zajmowałyby obie te same (pewne) pola
	private boolean zachodzą (Krotka k1, Krotka k2) {
		return (k1.wiersz < k2.wiersz + k2.postać.dajWysokość()
				&& k2.wiersz < k1.wiersz + k1.postać.dajWysokość()				
				&& k1.kolumna < k2.kolumna + k2.postać.dajSzerokość()
				&& k2.kolumna < k1.kolumna + k1.postać.dajSzerokość()
		);
	}
	
	// sprawdza, czy punkt (wiersz, kolumna) jest zajmowany przez postać z krotki k
	private boolean wewnątrz (Krotka k, int wiersz, int kolumna) {
		return (k.wiersz <= wiersz && wiersz < k.wiersz + k.postać.dajWysokość()
				&& k.kolumna <= kolumna && kolumna < k.kolumna + k.postać.dajSzerokość());
	}
	
	private int zmianaWiersza(Kierunek kierunek) {
		switch (kierunek) {
		case GÓRA:
    		return -1;
    	case DÓŁ:
    		return 1;
    	default:
    		return 0;
		}
	}
	
	private int zmianaKolumny(Kierunek kierunek) {
		switch (kierunek) {
		case LEWO:
			return -1;
		case PRAWO:
			return 1;
		default:
			return 0;
		}
	}
	
	// dfs używany przy sprawdzaniu, czy nie wystąpił deadlock
	private synchronized void dfs (Krotka rodzic) {
		
		odwiedzeni.add(rodzic.id);

		if (rodzic.wykonał == true || deadlock)
			return;
		
		int wierszMin = 0;
		int wierszMax = 0;
		int kolumnaMin = 0;
		int kolumnaMax = 0;
		
		Queue<Krotka> doOdwiedzenia = new ArrayDeque<Krotka>();
		Set<Krotka> wKolejce = new HashSet<Krotka>();
				
		switch (rodzic.ruch) {
		case LEWO:
		case PRAWO:
			wierszMin = rodzic.wiersz;
			wierszMax = rodzic.wiersz + rodzic.postać.dajWysokość() - 1;
			break;
		case DÓŁ:
		case GÓRA:
			kolumnaMin = rodzic.kolumna;
			kolumnaMax = rodzic.kolumna + rodzic.postać.dajSzerokość() - 1;
			break;
		}
		
		switch (rodzic.ruch) {
		case LEWO:
			kolumnaMin = kolumnaMax = rodzic.kolumna - 1;
			break;
		case PRAWO:
			kolumnaMin = kolumnaMax = rodzic.kolumna + rodzic.postać.dajSzerokość();
			break;
		case GÓRA:
			wierszMin = wierszMax = rodzic.wiersz - 1;
			break;
		case DÓŁ:
			wierszMin = wierszMax = rodzic.wiersz + rodzic.postać.dajWysokość();
			break;
		}
			
		for (int wiersz = wierszMin; wiersz <= wierszMax; ++wiersz) {
			for (int kolumna = kolumnaMin; kolumna <= kolumnaMax; ++kolumna) {
								
				for (Krotka k : naPlanszy) {
					if (k.postać != rodzic.postać && wewnątrz(k, wiersz, kolumna)) {
												
						if (odwiedzeni.contains(k.id)) {					
							// cykl w grafie opisanym przy definicji mapy 'odwiedzeni'
							deadlock = true;
						} else {
							if (!wKolejce.contains(k)) {
								doOdwiedzenia.add(k);
								wKolejce.add(k);
							}
						}
					}
				}
			}
		}
		
		while (!doOdwiedzenia.isEmpty()) {
			dfs(doOdwiedzenia.remove());
		}
	}
	
	private synchronized void sprawdźDeadlock(Krotka k) {
		odwiedzeni.clear();

		dfs(k);
	}
	
	public MojaPlansza(int wysokość, int szerokość) {
		
		// w treści zadanie niesprecyzowane, czy mamy akceptować zdegenerowane plansze
		assert(-1 < wysokość && -1 < szerokość);
		
		this.wysokość= wysokość;
		this.szerokość = szerokość;
	
		if (debug)
			System.out.println(this);
	}
	
	public synchronized void postaw(Postać postać, int wiersz, int kolumna) throws InterruptedException {
		
		boolean możnaPostawić = false;
		Krotka doPostawienia = new Krotka(postać, wiersz, kolumna, ilePostaci++);
		
		if (!mieściSię(doPostawienia))
			throw new IllegalArgumentException();
		
		for (Krotka k : naPlanszy)
			if (k.postać == postać)
				throw new IllegalArgumentException();
		
		while (!możnaPostawić) {
			możnaPostawić = true;
			
			for (Krotka k : naPlanszy) {
				if (zachodzą(k, doPostawienia)) {
					możnaPostawić = false;
					break;
				}
			}
			
			if (!możnaPostawić)
				wait();
		}
		
		naPlanszy.add(doPostawienia);
		
		if (debug)
			System.out.println(this);

	}

	public synchronized void przesuń(Postać postać, Kierunek kierunek)
		throws InterruptedException, DeadlockException {

		boolean możnaPrzesunąć = false;
		
		boolean zainicjalizowana = false;
		Krotka aktualna = new Krotka();
		
		for (Krotka k : naPlanszy) {
			if (k.postać == postać) {
				aktualna = k;
				zainicjalizowana = true;
				break;
			}
		}
		
		aktualna.ruch = kierunek;
		aktualna.wykonał = false;
		
		// taką postać będzie miała krotka aktualna po przesunięciu
		Krotka poZmianie = new Krotka(postać, aktualna.wiersz + zmianaWiersza(kierunek),
											aktualna.kolumna + zmianaKolumny(kierunek), 
											aktualna.id);
		
		if (!zainicjalizowana || !mieściSię(poZmianie))
    		throw new IllegalArgumentException();

		sprawdźDeadlock(aktualna);
		
		if (deadlock)
			throw new DeadlockException();
		
		while (!możnaPrzesunąć) {
			możnaPrzesunąć = true;
			
			for (Krotka k : naPlanszy) {
				if (k.postać != postać && zachodzą(k, poZmianie)) {
					możnaPrzesunąć = false;
					break;
				}
			}
			
			if (!możnaPrzesunąć)
				wait();
		}
		
		aktualna.wiersz += zmianaWiersza(kierunek);
		aktualna.kolumna += zmianaKolumny(kierunek);
		aktualna.wykonał = true;
		
		if (debug)
			System.out.println(this);
		
		notifyAll();
	}
	
	public synchronized void usuń(Postać postać) {
		
		boolean zainicjalizowana = false;
    	Krotka doUsunięcia = new Krotka();
	
    	for (Krotka k : naPlanszy) {
    		if (k.postać == postać) {
    			doUsunięcia = k;
    			zainicjalizowana = true;
    			break;
    		}
    	}
    	
    	if (!zainicjalizowana)
    		throw new IllegalArgumentException();
    	
    	naPlanszy.remove(doUsunięcia);
    	
    	if (debug)
    		System.out.println(this);
    	
    	notifyAll();
	
	}
	
	public synchronized void sprawdź (int wiersz, int kolumna,
            Akcja jeśliZajęte, Runnable jeśliWolne) {
		
		boolean wolne = true;
		
		if (wiersz < 0 || kolumna < 0 || wiersz >= wysokość || kolumna >= szerokość)
    		throw new IllegalArgumentException();

    	for (Krotka k : naPlanszy) {
    		if (wewnątrz(k, wiersz, kolumna)) {
    			wolne = false;
    			jeśliZajęte.wykonaj(k.postać);
    			break;
    		}
    	}
    	
    	if (wolne)
    		jeśliWolne.run();
	}
	
	// do testowania, zwraca czytelną reprezentację, o ile
	// liczba postaci, wysokość i szerokość są mniejsze od 100
	public String toString() {
		StringBuilder tmp = new StringBuilder();
		boolean błąd = false;
		int iluNa;
		
		tmp.append("|--|");
		
		for (int kolumna = 0; kolumna < szerokość; ++ kolumna) {
			if (kolumna < 10)
				tmp.append("." + kolumna + "|");
			else
				tmp.append(kolumna + "|");
		}
		tmp.append("\n");
		
		for (int wiersz = 0; wiersz < wysokość; ++wiersz) {
			tmp.append("|");
			
			if (wiersz < 10)
				tmp.append("." + wiersz);
			else
				tmp.append(wiersz);
			
			for (int kolumna = 0; kolumna < szerokość; ++kolumna) {
				tmp.append("|");
				
				// zmienna, mówiąca, ile postaci zajmuje miejsce (wiersz, kolumna)
				// dla poprawnie działającego programu jest mniejsza od 2
				iluNa = 0;
				for (Krotka k : naPlanszy) {
					if (wewnątrz(k, wiersz, kolumna)) {
						iluNa++;
						
						if (iluNa == 1) {
							if (k.id < 10)
								tmp.append("0");
							tmp.append(k.id);
						}
						else if (iluNa == 2) {
							błąd = true;
							tmp.setLength(tmp.length() - 1);
							// znak # pokazuje błędne miejsce
							tmp.append("##");
						}
					}
				}
				if (iluNa == 0)
					tmp.append("..");
			}
			tmp.append("|\n");
		}
		
		if (błąd)
			tmp.append("BŁĄD - więcej niż jedna postać na pozycjach #");
		
		return tmp.toString();
	}	
}
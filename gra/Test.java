package gra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

class Thread1 extends Thread {
	private Plansza plansza;
	
	public Thread1(Plansza plansza) {
		this.plansza = plansza;
	}
	
	public void run() {
		Pionek p = new Pionek(5, 3);
		try {
			plansza.postaw(p, 0, 17);
			plansza.przesuń(p, Kierunek.DÓŁ);
			for (int i = 0; i < 6; i++)
				plansza.przesuń(p, Kierunek.LEWO);
			plansza.sprawdź(2, 17, new Pisz(),new Wolne());
			plansza.sprawdź(3, 5, new Pisz(), new Wolne());
			plansza.przesuń(p, Kierunek.DÓŁ);
			sleep(1000);
			plansza.usuń(p);
		} catch (DeadlockException e) {
			System.out.println("Deadlock");
		} catch (Exception e) {
			System.out.println("ERROR");
		}		
	}
}

class Thread2 extends Thread {
	private Plansza plansza;
	private int wiersz;
	private int kolumna;
	private Kierunek kierunek;
	private int id;
	
	public Thread2(Plansza plansza, int wiersz, int kolumna, Kierunek kierunek, int id) {
		this.plansza = plansza;
		this.wiersz = wiersz;
		this.kolumna = kolumna;
		this.kierunek = kierunek;
		this.id = id;
	}
	
	public void run() {
		Pionek p = new Pionek(1, 1);
		try {
		plansza.postaw(p, wiersz, kolumna);
		sleep(1000);
		plansza.przesuń(p, kierunek);
		} catch (DeadlockException e) {
			try {
				sleep(1500);
			} catch (Exception e2) {}
			System.out.println(id + " " + p);
			System.out.println("Deadlock");
		} catch (Exception e) {
			System.out.println("Error");
		}
	}
}

class Thread3 extends Thread {
	private int id;
	private Plansza plansza;
	private int wiersz;
	private int kolumna;
	private int wysokość;
	private int szerokość;
	private Kierunek kierunek;
	
	public Thread3(int id, Plansza plansza, int wiersz, int kolumna, 
			int wysokość, int szerokość, Kierunek kierunek) {
		this.id = id;
		this.plansza = plansza;
		this.wiersz = wiersz;
		this.kolumna = kolumna;
		this.wysokość = wysokość;
		this.szerokość = szerokość;
		this.kierunek = kierunek;
	}
	
	public void run() {
		Pionek p = new Pionek(wysokość, szerokość);
		try {
		sleep(150);
		plansza.postaw(p, wiersz, kolumna);
		sleep(1000);
		plansza.przesuń(p, kierunek);
		} catch (DeadlockException e) {
			try {
				sleep(1500);
			} catch (Exception e2) {}
			System.out.println(id + " " + p);
			System.out.println("Deadlock");
		} catch (Exception e) {
			System.out.println("Error");
		}
	}
}

class Thread4 extends Thread {
	private int id;
	private Plansza plansza;
	private int wiersz;
	private int kolumna;
	private int wysokość;
	private int szerokość;
	private int steps;
	private Kierunek kierunek;
	public Thread4(int id, Plansza plansza, int wiersz, int kolumna, 
			int wysokość, int szerokość, Kierunek kierunek, int steps) {
		this.id = id;
		this.plansza = plansza;
		this.wiersz = wiersz;
		this.kolumna = kolumna;
		this.wysokość = wysokość;
		this.szerokość = szerokość;
		this.kierunek = kierunek;
		this.steps = steps;
	}
	
	public void run() {
		Pionek p = new Pionek(wysokość, szerokość);
		try {
			plansza.postaw(p, wiersz, kolumna);
			for (int i = 0; i < steps; i++) {
				plansza.przesuń(p, kierunek);
				sleep(1200);
			}
			if (kierunek == Kierunek.LEWO || kierunek == Kierunek.PRAWO)
				sleep(1200);
			else
				sleep(5000);
			Kierunek przeciwny = kierunek;
			switch(kierunek) {
			case GÓRA:
				przeciwny = Kierunek.DÓŁ; break;
			case DÓŁ:
				przeciwny = Kierunek.GÓRA; break;
			case LEWO:
				przeciwny = Kierunek.PRAWO; break;
			case PRAWO:
				przeciwny = Kierunek.LEWO; break;
			}
			
			for (int i = 0; i < steps; i++) {
				plansza.przesuń(p, przeciwny);
				sleep(1000);
			}
		} catch (Exception e) {
			System.out.println("ERROR");
		}
	}
}

class Thread5 extends Thread {
	private Plansza plansza;
	
	private static final int n = 14;
	
	private static ArrayList<Pionek> p = new ArrayList<Pionek>();
	
	public Thread5(Plansza plansza) {
		this.plansza = plansza;
		for (int i = 0; i < n; i++) {
			p.add(new Pionek(3, 3));
		}
	}
	
	public void run() {
		int ileWyłapanychBłędów = 0;
		for (int i = 0; i < n; i++) {
			try {
				działaj(i);
			} catch (IllegalArgumentException e) {
				ileWyłapanychBłędów++;
			} catch (Exception e) {
				System.out.println("ERROR");
			}
		}
		if (ileWyłapanychBłędów == n)
			System.out.println("OK");
	}
	
	private void działaj(int i) throws InterruptedException, DeadlockException {
		switch(i) {
		case 0:
			plansza.postaw(p.get(i), -1, 0); break;
		case 1:
			plansza.postaw(p.get(i), 0, -2); break;
		case 2:
			plansza.postaw(p.get(i), 6, 3); break;
		case 3:
			plansza.postaw(p.get(i), 5, 30); break;
		case 4:
			plansza.postaw(p.get(i), 0, 5); plansza.przesuń(p.get(i), Kierunek.GÓRA); break;
		case 5:
			plansza.postaw(p.get(i), 0, 0); plansza.przesuń(p.get(i), Kierunek.LEWO); break;
		case 6:
			plansza.postaw(p.get(i), 5, 0); plansza.przesuń(p.get(i), Kierunek.DÓŁ); break;
		case 7:
			plansza.postaw(p.get(i), 5, 17); plansza.przesuń(p.get(i), Kierunek.PRAWO); break;
		case 8:
			plansza.postaw(p.get(i), 0, 9); plansza.postaw(p.get(i), 0, 12); break;
		case 9:
			plansza.usuń(p.get(i)); break;
		case 10:
			plansza.sprawdź(-1, 0, new Pisz(),new Wolne()); break;
		case 11:
			plansza.sprawdź(0, -6, new Pisz(),new Wolne()); break;
		case 12:
			plansza.sprawdź(12, 0, new Pisz(),new Wolne()); break;
		case 13:
			plansza.sprawdź(0, 20, new Pisz(),new Wolne()); break;
		}
	}
}

public class Test {
	
	private static final int wysokość = 7;
	private static final int szerokość = 20;
	
	private static Plansza plansza = new MojaPlansza(wysokość, szerokość);
	
	private static ArrayList<Integer> kolejność = new ArrayList<Integer>();
	private static ArrayList<Thread> wątki = new ArrayList<Thread>();
	
	// test postaci idących po tej samej ścieżce i usuwających się na jej końcu
	private static void test1() {
		for (int i = 0; i < 5; i++)
			wątki.add(new Thread1(plansza));
		testuj();
	}
	
	// test na deadlock: postacie stoją w kwadracie
	private static void test2() {
		wątki.add(new Thread2(plansza, 0, 0, Kierunek.PRAWO, 0));
		wątki.add(new Thread2(plansza, 0, 1, Kierunek.DÓŁ, 1));
		wątki.add(new Thread2(plansza, 1, 1, Kierunek.LEWO, 2));
		wątki.add(new Thread2(plansza, 1, 0, Kierunek.GÓRA, 3));
		testuj();
	}
	
	// test na deadlock: postacie idą w przeciwne strony
	private static void test3() {
		wątki.add(new Thread3(0, plansza, 2, 5, 3, 4, Kierunek.PRAWO));
		wątki.add(new Thread3(1, plansza, 3, 9, 1, 1, Kierunek.LEWO));
		testuj();
	}

	// test na deadlock: postacie po ruchu zahaczałyby o siebie pojedynczą pozycją
	private static void test4() {
		wątki.add(new Thread3(0, plansza, 2, 5, 3, 4, Kierunek.PRAWO));
		wątki.add(new Thread3(1, plansza, 3, 9, 1, 1, Kierunek.DÓŁ));
		wątki.add(new Thread3(2, plansza, 4, 9, 2, 5, Kierunek.LEWO));
		testuj();
	}
	
	// test na deadlock: postacie po ruchu zahaczałyby o siebie, niekoniecznie pojedynczą pozycją
	private static void test5() {
		wątki.add(new Thread3(0, plansza, 2, 5, 3, 4, Kierunek.PRAWO));
		wątki.add(new Thread3(1, plansza, 4, 9, 1, 1, Kierunek.DÓŁ));
		wątki.add(new Thread3(2, plansza, 5, 8, 2, 5, Kierunek.LEWO));
		wątki.add(new Thread3(3, plansza, 5, 2, 1, 6, Kierunek.GÓRA));
		testuj();
	}
	
	// test postaci wchodzących sobie w drogę
	private static void test6() {
		int n = 3;
		int m = 6;
		int initialx = 1;
		int initialy = 3;
		int vertsteps = 5;
		int horsteps = 8;
		for (int i = 0; i < n; i++) {
			wątki.add(new Thread4(i, plansza, initialx + i + 2, initialy, 1, 1, Kierunek.PRAWO, horsteps));
		}
		for (int i = 0; i < m; i++) {
			wątki.add(new Thread4(i + m, plansza, initialx, initialy + i + 2, 1, 1, Kierunek.DÓŁ, vertsteps));
		}
		testuj();
	}
	
	// test raportów o błędach innych niż deadlock
	private static void test7() {
		wątki.add(new Thread5(plansza));
		testuj();
	}
	
	private static void testuj() {
		int ile = wątki.size();
		
		for (int i = 0; i < ile; i++) {
			kolejność.add(i);
		}
		long seed = System.nanoTime();
		Collections.shuffle(kolejność, new Random(seed));
		
		System.out.println("\n" + kolejność);
		
		for (int i = 0; i < ile; i++) {
			wątki.get(kolejność.get(i)).start();
		}
		
		for (int i = 0; i < ile; i++) {
			try {
				wątki.get(i).join();
			} catch (Exception e) {}
		}
		
		System.out.println("\nKoniec!");
	}
	
	public static void main(String[] args) {
		
		// żeby drukować planszę, należy w MojaPlansza ustawić wartość 
		// zmiennej debug na true
		
		test1();
		//test2();
		//test3();
		//test4();
		//test5();
		// test6 wykonuje się dłużej ze względu na sleep'y, zalecane jest
		// włączenie wyświetlania mapy
		//test6();
		//test7();
	}
}